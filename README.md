Raspberry project
=====================

### Description

* Goal: Remote car controller on Raspberry pi.
	* The Android (/raspberry-android), iOS (/raspberry-ios) or Java (/raspberry-android/java) apps send HTTP Post requests to control the servo or listen captors.
	* The PHP Rest API (/raspberry-android/api) forwards the requests to the raspberry on board server (/raspberry-on-board).
	* The raspberry on board server (/raspberry-on-board) controls the pin, i2c, servo...
* Location: Paris
* Starting Date: August 2015


### Structure

* /raspberry-android:   Android app and PHP Rest API in order to control the raspberry. Android Studio project.
    * /api:             PHP Rest API.
    * /app:             The Android app.
    * /java:			The Java desktop app.
    * /shared:          Common code to the Android app and the java app.
* /raspberry-ios:       iOS app and PHP Rest API in order to control the raspberry. xCode project.
* /raspberry-on-board:  Raspberry code. Listen the port 8888 and control pin.
    * server.cpp:       Create the socket, and parse json.
    * /rapidjson:       Library to parse json.
    * /wiringPi:        Control pin.
    * /pca:             Control pin.
    * hadware.cpp:      Control pin.
    

### Raspberry format

* "succeed": boolean
* "toast": string
* "debug": string
* "content": JSON object
    * "user": JSON object (optional)
    * "date_request":"2015-08-11 09:53:55"
    * "hardware": JSON Array
        * "id": int
            * 1 == ID_LED_1
            * 2 == ID_DISTANCE_1
            * 3 == ID_DISTANCE_2
            * 4 == ID_SERVO_1
            * 5 == ID_SERVO_2
        * "read": boolean (false is write)
        * "value": string (contains double)
        * "type": string (led, servo)
        * "succeeded": boolean
    

### TODO before use

* Clone the project. Create a crypt.json file in /raspberry-android/config/crypt/ (to configure the crypto between the PHP API and the clients: android, ios, java app) to compile android or java app.
* Clone the project, compile and run the server on the raspberry (see /raspberry-on-board/README.md)
* Clone the project on your PHP server (and add the crypt.json)


## DEVELOPERS

* Mercandalli J.
* Mercandalli E.
