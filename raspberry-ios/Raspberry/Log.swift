//
//  Log.swift
//  Raspberry
//
//  Created by Jonathan on 18/08/16.
//  Copyright © 2016 Jonathan. All rights reserved.
//

import Foundation

class Log {
    
    static func d(logMessage: String) {
        print(logMessage);
    }
}