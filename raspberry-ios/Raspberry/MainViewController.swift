//
//  ViewController.swift
//  Raspberry
//
//  Created by Jonathan on 06/03/16.
//  Copyright © 2016 Jonathan. All rights reserved.
//

import Foundation

import UIKit


class MainViewController: UIViewController, TimerManagerListener, OutputManagerListener {
    // MARK: Properties
    
    @IBOutlet weak var titleTextField: UITextField!
    @IBOutlet weak var subtitleTextField: UITextField!
    @IBOutlet weak var pickerView: UIPickerView!
    @IBOutlet weak var doneButton: UIBarButtonItem!
    
    var mBackground: UIView!
    
    var mTimeLabel: UILabel!
    
    // MARK: View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        mBackground = UIView();
        mBackground.backgroundColor = ColorManager.sInstance.getBackground();
        mBackground.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)
        self.view.addSubview(mBackground);
        
        addLabel();
        addButtons();
    }
    
    override func viewWillAppear(animated: Bool) {
        TimerManager.sInstance.addTimerManagerListener(self);
        OutputManager.sInstance.addOutputManagerListener(self);
    }
    
    override func viewWillDisappear(animated: Bool) {
        TimerManager.sInstance.removeAllTimerManagerListeners();
        OutputManager.sInstance.removeOutputManagerListener();
    }
    
    func addLabel() {
        mTimeLabel = UILabel();
        mTimeLabel.frame = CGRectMake(0, 70, self.view.frame.size.width, 100);
        mTimeLabel.textAlignment = .Center;
        mTimeLabel.font = UIFont(name: "HelveticaNeue-Light", size: 74.0);
        mTimeLabel.textColor = ColorManager.sInstance.getWhite();
        syncTimeLabel();
        self.view.addSubview(mTimeLabel);
    }
    
    func addButtons() {
        
        let xPosition: CGFloat = (self.view.frame.size.width - 200) / 2;
        
        let button = UIButton(type: UIButtonType.System) as UIButton;
        button.frame = CGRectMake(xPosition, 200, 200, 46);
        button.backgroundColor = ColorManager.sInstance.getGreen();
        button.titleLabel!.font = UIFont(name: "HelveticaNeue", size: 20.0);
        button.setTitle("Start / Stop", forState: UIControlState.Normal);
        button.addTarget(self, action: #selector(MainViewController.startStop(_:)), forControlEvents: UIControlEvents.TouchUpInside);
        button.setTitleColor(UIColor.whiteColor(), forState: .Normal);
        button.setTitleColor(UIColor.grayColor(), forState: .Selected);
        self.view.addSubview(button);
        
        let buttonReset = UIButton(type: UIButtonType.System) as UIButton;
        buttonReset.frame = CGRectMake(xPosition, 260, 200, 46);
        buttonReset.backgroundColor = ColorManager.sInstance.getRed();
        buttonReset.titleLabel!.font = UIFont(name: "HelveticaNeue", size: 20.0);
        buttonReset.setTitle("Reset", forState: UIControlState.Normal);
        buttonReset.addTarget(self, action: #selector(MainViewController.reset(_:)), forControlEvents: UIControlEvents.TouchUpInside);
        buttonReset.setTitleColor(UIColor.whiteColor(), forState: .Normal);
        buttonReset.setTitleColor(UIColor.grayColor(), forState: .Selected);
        self.view.addSubview(buttonReset);
    }
    
    func startStop(sender:UIButton!) {
        TimerManager.sInstance.startStop();
    }
    
    func reset(sender:UIButton!) {
        TimerManager.sInstance.reset();
        OutputManager.sInstance.reset();
        
        ApiManager.sInstance.setLedOn(1, on: true);
    }

    /**
     * Override
     */
    func onTimerUpdate(durationFromStart: TimerManagerDuration) {
        syncTimeLabel();
    }
    
    /**
     * Override
     */
    func onOutputChanged(output: String) {
    
    }
    
    func syncTimeLabel() {
        mTimeLabel.text = TimerManager.sInstance.getStringCurrentTime();
    }
}
