//
//  ColorManager.swift
//  Raspberry
//
//  Created by Jonathan on 18/08/16.
//  Copyright © 2016 Jonathan. All rights reserved.
//

import Foundation

import UIKit

class ColorManager {
    
    static let sInstance = ColorManager();
    
    internal var mBackground: UIColor;
    internal var mWhite: UIColor;
    internal var mBlack: UIColor;
    internal var mGreen: UIColor;
    internal var mRed: UIColor;
    
    private init () {
        mBackground = UIColor(netHex: 0x303030);
        mWhite = UIColor.whiteColor();
        mBlack = UIColor.blackColor();
        mGreen = UIColor(netHex: 0x009688);
        mRed = UIColor(netHex: 0xDD2C00);
    }
    
    func getBackground() -> UIColor {
        return mBackground;
    }
    
    func getWhite() -> UIColor {
        return mWhite;
    }
    
    func getBack() -> UIColor {
        return mBlack;
    }
    
    func getGreen() -> UIColor {
        return mGreen;
    }
    
    func getRed() -> UIColor {
        return mRed;
    }
}

extension UIColor {
    convenience init(red: Int, green: Int, blue: Int) {
        assert(red >= 0 && red <= 255, "Invalid red component")
        assert(green >= 0 && green <= 255, "Invalid green component")
        assert(blue >= 0 && blue <= 255, "Invalid blue component")
        
        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
    }
    
    convenience init(netHex:Int) {
        self.init(red:(netHex >> 16) & 0xff, green:(netHex >> 8) & 0xff, blue:netHex & 0xff)
    }
}