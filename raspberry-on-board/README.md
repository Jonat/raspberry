Robotics - Car - API
=====================

**_Unfinished project, still in development_**


## PROJECT DESCRIPTION

* Name : Robotics-Car
* Description : RestAPI server for Raspberry Pi
* Location : Paris
* Starting Date : August 2015


## INSTALLATION

* Step 1: Clone git repository

* Open a terminal
* Type the following commands to launch the server to listen commands
    * ```git clone https://Jonat@bitbucket.org/Jonat/raspberry.git```
    * ```cd raspberry```
    * ```make```
    * ```./server```

* Step 2 : optionnal

Action on the Raspberry terminal :
sudo crontab -e

At the bottom of the file, add :
@reboot sudo /home/pi/Robotics-Car-API/server


## REST API ROUTES

### Main Controller

|Root      | Method   | Description   | Input    | Output
|----------|----------|---------------|----------|----------
| :8888    | POST     |               |          | 


### JSON Format: Type

* "succeed": boolean
* "toast": string
* "debug": string
* "content": JSON object
    * "user": JSON object (optional)
    * "date_request":"2015-08-11 09:53:55"
    * "hardware": JSON Array
        * "id": int
            * 1 == ID_LED_1
            * 2 == ID_DISTANCE_1
            * 3 == ID_DISTANCE_2
            * 4 == ID_SERVO_1
            * 5 == ID_SERVO_2
        * "read": boolean (false is write)
        * "value": string (contains double)
        * "type": string (led, servo)
        * "succeeded": boolean


### JSON Format: Example 

```json
{  
    "succeed":true,
    "toast":"Test toast",
    "debug":"on\/off led",
    "content": {  
        "user": {  
            "id":1,
            "username":"Jonathan"
        },
        "date_request":"2015-08-11 09:53:55",
        "hardware": [  
            {  
                "id":1,
                "read":false,
                "value":"0",
                "type":"led",
                "succeed":true
            },
            {  
                "id":4,
                "read":false,
                "value":"0.5",
                "type":"servo",
                "succeed":true
            },
            {  
                "id":5,
                "read":false,
                "value":"0.5",
                "type":"servo",
                "succeed":true
            }
        ],
        "init_hardware":false
    }
}
```


## DEVELOPERS

* Mercande
* Mercandj


## LICENSE

OpenSource : just mention developer name if you use the code.

