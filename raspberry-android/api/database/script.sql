SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema raspberry
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `raspberry` ;
CREATE SCHEMA IF NOT EXISTS `raspberry` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci ;
SHOW WARNINGS;
USE `raspberry` ;

-- -----------------------------------------------------
-- Table `User`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `User` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `User` (
	`id`                         INT NOT NULL AUTO_INCREMENT,
	`username`                   VARCHAR(100) NOT NULL,
	`password`                   VARCHAR(100) NOT NULL,
	`date_creation`              DATETIME NULL,
	`date_last_connection`       DATETIME NULL,
	`first_name`                 VARCHAR(100) NULL,
	`last_name`                  VARCHAR(100) NULL,
	`email`                      VARCHAR(100) NULL,
	`age`                        TINYINT NULL,
	`sex`                        TINYINT(1) NULL,
	`admin`                      TINYINT(1) NOT NULL DEFAULT '0',
	`id_file_profile_picture`    INT NULL,
	`description`                VARCHAR(999) NULL,
	`language`                   VARCHAR(50) NULL,
	`longitude`                  VARCHAR(80) NULL,
	`latitude`                   VARCHAR(80) NULL,
	`android_apk_version`        VARCHAR(45) NULL,
	`android_sdk`                VARCHAR(60) NULL,
	`android_id`                 VARCHAR(600) NULL,
	PRIMARY KEY (`id`))
ENGINE = MyISAM;

SHOW WARNINGS;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

-- -----------------------------------------------------
-- Command to add
-- -----------------------------------------------------
-- ALTER TABLE `Device` ADD `base_version` VARCHAR(300) NULL
-- ALTER TABLE `Event` ADD `base_version` VARCHAR(300) NULL
--
-- INSERT INTO `Application` (`android_app_name`, `android_app_package`, `operating_system`, `google_api_key`, `android_app_version_code`, `android_app_version_code_last_supported`, `android_app_version_name`) VALUES ('Raspberry', 'com.mercandalli.android.apps.raspberry', 'Android', 'AIzaSyAEd2E465jHFUhdnLBC3LGmwe20-uo3Edg', 1, 1, '1.0.0');