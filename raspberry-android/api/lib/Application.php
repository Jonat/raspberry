<?php
namespace lib;
use \app\manager\ConfigManager;

abstract class Application {
	public $_config;
	public $_pdo;
	public $_router;

	public function __construct() {
		$this->_config =  ConfigManager::getInstance();
		$this->_pdo = (new Connexion($this))->getPDO();	
		$this->_router = new Router($this);
	}

	abstract public function run();
}