<?php
namespace app\controller;
use \app\model\User;
use \lib\HTTPRequest;
use \lib\HTTPResponse;

class RoboticsController extends \lib\Controller {

	// https://github.com/projectweekend/Pi-GPIO-Server
	/**
	 * Robotics : read pin value
	 * @uri    /robotics
	 * @method GET
	 * @return JSON with info about robotics
	 * @deprecated
	 */
	public function get($pin_id) {
		$json['succeed'] = false;

		$id_user = $this->_app->_config->getId_user();
		$userManager = $this->getManagerof('User');
		$user = $userManager->getById($id_user);
		if($user->isAdmin()) {
			$response = file_get_contents($this->_app->_config->get('server_robotics_1') . "/api/v1/pin/".$pin_id);
			$json['succeed'] = true;
			$json['result'] = array(
				array(
					"title" => "response",
					"value" => "".$response
				)
			);
		}
		else {
			$json['toast'] = 'Unauthorized access.';
		}
		HTTPResponse::send(json_encode($json));
	}

	/**
	 * Do robotics actions
	 * @uri    	/robotics
	 * @method 	POST
	 * @deprecated
	 */
	public function post($pin_id) {
		$json['succeed'] = false;

		$id_user = $this->_app->_config->getId_user();
		$userManager = $this->getManagerof('User');
		$user = $userManager->getById($id_user);
		if ($user->isAdmin()) {
			$value = '0';

			if (HTTPRequest::postExist('value')) {
				$value = HTTPRequest::postData('value');
            }

			$servo = false;
			if (HTTPRequest::postExist('servo')) {
				$servo = HTTPRequest::postData('servo');
            }

			$url = $this->_app->_config->get('server_robotics_1') . "/api/v1/pin/".$pin_id;
			$data = array('value' => $value);

			$options = array(
			    'http' => array(
			        'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
			        'method'  => 'PATCH',
			        'content' => http_build_query($data),
			    )
			);
			$context  = stream_context_create($options);
			$response = file_get_contents($url, false, $context);

			$json['succeed'] = true;
			$json['result'] = array(
				array(
					"title" => "response",
					"value" => "".$response
				)
			);
		}
		else {
			$json['toast'] = 'Unauthorized access.';
		}
		HTTPResponse::send(json_encode($json));
	}

	/**
	 * Do robotics actions
	 * @uri    	/robotics
	 * @method 	POST
	 */
	public function raspberry() {
		$json['succeed'] = false;

		$cryptManager = $this->getManagerOf('Crypt');
		$a_gain = $this->_app->_config->getAuthentication('a_gain');
		$b_offset = $this->_app->_config->getAuthentication('b_offset');
		$c_mod = $this->_app->_config->getAuthentication('c_mod');

        $inputBrut = file_get_contents('php://input');
        $inputJSON = $cryptManager->decryptCustom(
            $inputBrut,
            $a_gain,
            $b_offset,
            $c_mod);
        $input = json_decode($inputJSON, TRUE);

        // Get the raspberry content to send.
        if (is_null($input) || !array_key_exists('raspberry-content', $input)) {
            $json['debug'] = 'There is an error. $inputJSON = ' . $inputJSON;
            $json['debug-input'] = $input;
		    HTTPResponse::send(json_encode($json));
            return;
        }
        $raspberry_content = $input['raspberry-content'];

        // Get the raspberry local ip.
        $url = $this->_app->_config->get('server_robotics_2');
        if (array_key_exists('raspberry-ip', $input)) {
            $url = $input['raspberry-ip'];
        }

        $options = array(
            'http' => array(
                'protocol_version' => 1.1,
                'header'  => "Content-type: application/json\r\n".
                             "Content-length: " . strlen(json_encode($raspberry_content)) . "\r\n",
                'method'  => 'POST',
                'content' => json_encode($raspberry_content),
                'timeout' => 4
            )
        );
        $context  = stream_context_create($options);
        $response_content = json_decode(@file_get_contents($url, false, $context), TRUE);

        if (is_array($response_content)) {
            $json['succeed'] = true;
            $json['raspberry-content'] = $response_content;
            $json['raspberry-connected'] = true;
        } else {
            $json['succeed'] = false;
            $json['raspberry-connected'] = false;
        }
        $json['debug-json-nuc-request'] = $raspberry_content;

		HTTPResponse::send(json_encode($json));
	}
}
