<?php
namespace app;

class RESTapi extends \lib\Application {

	public function run() {
		
		$this->_router

			->post('/robotics','Robotics#raspberry')

			// Deprecated
			->get('/robotics/:id','Robotics#get')

			// Deprecated
			->post('/robotics/:id','Robotics#post')

			->otherwise(function() {
				\lib\HTTPResponse::redirect404();
			});
		
		exit();
	}
}
