<?php
namespace app\manager;

class CryptManager {
	protected static $instance;

	public static function getInstance() {
		if (!isset(static::$instance)){
			static::$instance = new static();
		}
		return static::$instance;
	}

	private function __construct() {
	}

	public function encryptCustom($string='', $gain, $offset, $mod) {
	    $string = base64_encode($string);
        $newstring = '';
        for ($i=0 ; $i < strlen($string) ; $i++) {
            $old = ord($string[$i]);
            if($old >= $mod) {
                $newstring .= $string[$i];
            } else {
                $new =  $this->mod($old + ($gain * $i + $offset), $mod);
                $newstring .= chr($new);
            }
        }
        return $newstring;
    }

    public function decryptCustom($string='', $gain, $offset, $mod) {
        $newstring = '';
        for ($i=0 ; $i < strlen($string) ; $i++) {
            $old = ord($string[$i]);
            if($old >= $mod) {
                $newstring .= $string[$i];
            } else {
                $new =  $this->mod($old - ($gain * $i + $offset), $mod);
                $newstring .= chr($new);
            }
        }
        return base64_decode($newstring);
    }

    private function mod($a, $n) {
        return $a < 0 ? ($a % $n + $n) % $n : $a % $n;
    }
}