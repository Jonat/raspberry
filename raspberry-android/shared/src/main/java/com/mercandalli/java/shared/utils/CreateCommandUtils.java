package com.mercandalli.java.shared.utils;

import android.support.annotation.FloatRange;
import android.support.annotation.NonNull;

import com.mercandalli.java.shared.model.Content;
import com.mercandalli.java.shared.model.Hardware;
import com.mercandalli.java.shared.model.PhpCommand;
import com.mercandalli.java.shared.model.RaspberryCommand;

import java.util.ArrayList;
import java.util.List;

public class CreateCommandUtils {

    @NonNull
    public static PhpCommand createWriteLedCommand(
            @Hardware.HardwareLedId final int id,
            final boolean on) {
        final List<Hardware> hardwareList = new ArrayList<>();
        hardwareList.add(new Hardware(id, true, false, on, Hardware.LED));
        return new PhpCommand(new RaspberryCommand(new Content(hardwareList)));
    }

    @NonNull
    public static PhpCommand createWriteServoCommand(
            @Hardware.HardwareServoId final int id,
            @FloatRange(from = 0.0, to = 1.0) final float value) {
        final List<Hardware> hardwareList = new ArrayList<>();
        hardwareList.add(new Hardware(id, true, false, value, Hardware.SERVO));
        return new PhpCommand(new RaspberryCommand(new Content(hardwareList)));
    }
}
