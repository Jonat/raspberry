package com.mercandalli.java.shared.model;

import android.support.annotation.Nullable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Content {

    @Nullable
    @SerializedName("hardware")
    private final List<Hardware> mHardwareList;

    public Content(@Nullable final List<Hardware> hardwareList) {
        mHardwareList = hardwareList;
    }

    @Nullable
    public List<Hardware> getHardwareList() {
        return mHardwareList;
    }

    @Override
    public String toString() {
        return "Content{" +
                "mHardwareList=" + mHardwareList +
                '}';
    }
}
