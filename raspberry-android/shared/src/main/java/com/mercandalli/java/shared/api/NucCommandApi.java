package com.mercandalli.java.shared.api;

import android.support.annotation.FloatRange;
import android.support.annotation.NonNull;

import com.mercandalli.java.shared.model.Hardware;
import com.mercandalli.java.shared.model.PhpCommand;

public interface NucCommandApi {

    void sendCommand(@NonNull final PhpCommand command);

    void sendCommand(@Hardware.HardwareLedId final int id, @NonNull final PhpCommand command);

    void writeLed(@Hardware.HardwareLedId final int id, final boolean on);

    void writeServo(@Hardware.HardwareServoId final int id, @FloatRange(from = 0.0, to = 1.0) final float value);
}
