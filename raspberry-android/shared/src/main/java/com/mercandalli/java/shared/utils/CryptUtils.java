package com.mercandalli.java.shared.utils;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import static com.mercandalli.java.shared.utils.StringUtils.isEmpty;

@SuppressWarnings("unused")
public final class CryptUtils {

    private CryptUtils() {
        // Non-instantiable.
    }

    /**
     * n = (o + (i * a + b)) % c
     * <p/>
     * Where:
     * <ul>
     * <li>n == new char</li>
     * <li>o == old char</li>
     * <li>i == position</li>
     * <li>a == key 1</li>
     * <li>b == key 2</li>
     * <li>c == alphabet size</li>
     * </ul>
     */
    @NonNull
    public static String encryptCustom(
            @Nullable String clear,
            final int gain, // a
            final int offset, // b
            final int mod) { // c (alphabet size)
        if (isEmpty(clear)) {
            return "";
        }
        final String base64 = Base64Utils.encode(clear);
        final StringBuilder sb = new StringBuilder();
        for (int i = 0, size = base64.length(); i < size; i++) {
            final char oldCharC = base64.charAt(i);
            final int oldCharInt = (int) oldCharC;
            if (oldCharInt >= mod) {
                sb.append(oldCharC);
            } else {
                final int newCharInt = mod(oldCharInt + (gain * i + offset), mod);
                sb.append((char) newCharInt);
            }
        }
        return sb.toString();
    }

    /**
     * n = (o - (i * a + b)) % c
     * <p/>
     * Where:
     * <ul>
     * <li>n == new char</li>
     * <li>o == old char</li>
     * <li>i == position</li>
     * <li>a == key 1</li>
     * <li>b == key 2</li>
     * <li>c == alphabet size</li>
     * </ul>
     */
    @NonNull
    public static String decryptCustom(
            @Nullable final String crypt,
            final int gain, // a
            final int offset, // b
            final int mod) { // c (alphabet size)
        if (isEmpty(crypt)) {
            return "";
        }
        final StringBuilder sb = new StringBuilder();
        for (int i = 0, size = crypt.length(); i < size; i++) {
            final char oldCharC = crypt.charAt(i);
            final int oldCharInt = (int) oldCharC;
            if (oldCharInt >= mod) {
                sb.append(oldCharC);
            } else {
                final int newCharInt = mod(oldCharInt - (gain * i + offset), mod);
                sb.append((char) newCharInt);
            }
        }
        return Base64Utils.decode(sb.toString());
    }

    private static int mod(int a, int n) {
        return a < 0 ? (a % n + n) % n : a % n;
    }
}
