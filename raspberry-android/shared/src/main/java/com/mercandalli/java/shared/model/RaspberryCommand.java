package com.mercandalli.java.shared.model;

import android.support.annotation.Nullable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Will be send to the raspberry and parsed by the raspberry.
 */
public class RaspberryCommand {

    @Nullable
    @SerializedName("content")
    private final Content mContent;

    @SerializedName("succeed")
    private final boolean mIsSucceeded;

    public RaspberryCommand(
            @Nullable final Content content) {
        mContent = content;
        mIsSucceeded = true;
    }

    @Nullable
    public Content getContent() {
        return mContent;
    }

    @Nullable
    public List<Hardware> getHardwareList() {
        if (mContent == null) {
            return null;
        }
        return mContent.getHardwareList();
    }

    public boolean isSucceeded() {
        return mIsSucceeded;
    }

    @Override
    public String toString() {
        return "RaspberryCommand{" +
                "mContent=" + mContent +
                ", mIsSucceeded=" + mIsSucceeded +
                '}';
    }
}
