package com.mercandalli.java.shared.model;

import com.google.gson.annotations.SerializedName;

public class PhpResponse {

    @SerializedName("succeed")
    private final boolean mSucceeded;

    @SerializedName("raspberry-connected")
    private final boolean mRaspberryConnected;

    public PhpResponse(final boolean succeeded, final boolean raspberryConnected) {
        mSucceeded = succeeded;
        mRaspberryConnected = raspberryConnected;
    }

    public boolean isSucceeded() {
        return mSucceeded;
    }

    public boolean isRaspberryConnected() {
        return mRaspberryConnected;
    }
}
