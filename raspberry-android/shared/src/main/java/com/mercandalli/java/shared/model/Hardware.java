package com.mercandalli.java.shared.model;

import android.support.annotation.FloatRange;
import android.support.annotation.IntDef;
import android.support.annotation.Nullable;
import android.support.annotation.StringDef;

import com.google.gson.annotations.SerializedName;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public class Hardware {

    @IntDef({
            ID_LED_1,
            ID_DISTANCE_1,
            ID_DISTANCE_2,
            ID_SERVO_1_DIRECTION,
            ID_SERVO_2_ENGINE})
    @Retention(RetentionPolicy.SOURCE)
    public @interface HardwareId {
    }

    @IntDef({ID_LED_1})
    @Retention(RetentionPolicy.SOURCE)
    public @interface HardwareLedId {
    }

    @IntDef({
            ID_SERVO_1_DIRECTION,
            ID_SERVO_2_ENGINE})
    @Retention(RetentionPolicy.SOURCE)
    public @interface HardwareServoId {
    }

    public static final int ID_LED_1 = 1;
    public static final int ID_DISTANCE_1 = 2;
    public static final int ID_DISTANCE_2 = 3;
    public static final int ID_SERVO_1_DIRECTION = 4;
    public static final int ID_SERVO_2_ENGINE = 5;

    @StringDef({LED, SERVO})
    @Retention(RetentionPolicy.SOURCE)
    public @interface Type {
    }

    public static final String LED = "led";

    public static final String SERVO = "servo";

    @HardwareId
    @SerializedName("id")
    private final int mId;

    @SerializedName("succeed")
    private final boolean mSucceed;

    @SerializedName("read")
    private final boolean mRead;

    @Nullable
    @SerializedName("value")
    private final String mValue;

    @Nullable
    @SerializedName("type")
    private final String mType;

    public Hardware(
            @HardwareId final int id,
            final boolean succeed,
            final boolean read,
            @Nullable final String value,
            @Nullable @Type final String type) {
        mId = id;
        mSucceed = succeed;
        mRead = read;
        mValue = value;
        mType = type;
    }

    public Hardware(
            @HardwareId final int id,
            final boolean succeed,
            final boolean read,
            @FloatRange(from = 0.0, to = 1.0) final float value,
            @Nullable @Type final String type) {
        this(id, succeed, read, String.valueOf(value), type);
    }

    public Hardware(
            @HardwareId final int id,
            final boolean succeed,
            final boolean read,
            final boolean value,
            @Nullable @Type final String type) {
        this(id, succeed, read, value ? 1 : 0, type);
    }

    @HardwareId
    public int getId() {
        return mId;
    }

    public boolean isSucceed() {
        return mSucceed;
    }

    public boolean isRead() {
        return mRead;
    }

    @Nullable
    public String getValue() {
        return mValue;
    }

    @Nullable
    public String getType() {
        return mType;
    }

    @Override
    public String toString() {
        return "Hardware{" +
                "mId=" + mId +
                ", mSucceed=" + mSucceed +
                ", mRead=" + mRead +
                ", mValue='" + mValue + '\'' +
                ", mType='" + mType + '\'' +
                '}';
    }
}
