package com.mercandalli.java.shared.model;

import android.support.annotation.Nullable;

import com.google.gson.annotations.SerializedName;

/**
 * Will be send to the raspberry and parsed by the raspberry.
 */
public class PhpCommand {

    @Nullable
    @SerializedName("raspberry-content")
    private final RaspberryCommand mRaspberryCommand;

    @Nullable
    @SerializedName("raspberry-ip")
    private final String mRaspberryIp;

    public PhpCommand(
            @Nullable final RaspberryCommand raspberryCommand,
            @Nullable final String raspberryIp) {
        mRaspberryCommand = raspberryCommand;
        mRaspberryIp = raspberryIp;
    }

    public PhpCommand(
            @Nullable final RaspberryCommand raspberryCommand) {
        this(raspberryCommand, null);
    }

    @Override
    public String toString() {
        return "PhpCommand{" +
                "mRaspberryCommand=" + mRaspberryCommand +
                ", mRaspberryIp='" + mRaspberryIp + '\'' +
                '}';
    }
}
