package com.mercandalli.android.apps.raspberry;

import android.support.annotation.FloatRange;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.mercandalli.android.apps.raspberry.gas.GasButtonManager;
import com.mercandalli.android.apps.raspberry.api.PhpApiManager;
import com.mercandalli.java.shared.model.Hardware;

public class EngineManager implements GasButtonManager.OnButtonPressedChangeListener {

    /**
     * Singleton instance.
     */
    @Nullable
    private static EngineManager sInstance;

    /**
     * Get the singleton instance.
     */
    @NonNull
    public static EngineManager getInstance() {
        if (sInstance == null) {
            sInstance = new EngineManager();
        }
        return sInstance;
    }

    @NonNull
    private final GasButtonManager mGasButtonManager = GasButtonManager.getInstance();

    @NonNull
    private final PhpApiManager mPhpApiManager = PhpApiManager.getInstance();

    private EngineManager() {
    }

    public void initialize() {
        mGasButtonManager.addOnButtonPressedChangeListener(this);
    }

    @Override
    public boolean onGasButtonDown(@FloatRange(from = 0.0, to = 1.0) final float gasPercent) {
        mPhpApiManager.writeServo(Hardware.ID_SERVO_2_ENGINE, gasPercent);
        return false;
    }

    @Override
    public boolean onGasButtonMove(@FloatRange(from = 0.0, to = 1.0) final float gasPercent) {
        return false;
    }

    @Override
    public boolean onGasButtonUp(@FloatRange(from = 0.0, to = 1.0) final float gasPercent) {
        mPhpApiManager.writeServo(Hardware.ID_SERVO_2_ENGINE, gasPercent);
        return false;
    }
}
