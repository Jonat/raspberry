package com.mercandalli.android.apps.raspberry.accelerometer;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.support.annotation.FloatRange;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.mercandalli.android.library.base.precondition.Preconditions;

import java.util.ArrayList;
import java.util.List;

/**
 * Simple manager getting the accelerometer "y" position.
 */
public final class AccelerometerManager {

    /**
     * Needs to be != 0.
     * <p>
     * Between ] 0f ; 10f [
     */
    private static final float ACCELEROMETER_X_RANGE = 3.6f;

    /**
     * Singleton instance.
     */
    @Nullable
    private static AccelerometerManager sInstance;

    /**
     * Get the singleton instance.
     */
    @NonNull
    public static AccelerometerManager getInstance(@NonNull final Context context) {
        if (sInstance == null) {
            sInstance = new AccelerometerManager(context);
        }
        return sInstance;
    }

    private boolean mIsStarted = false;

    // region Private, non null and final fields.
    /**
     * Used to get the accelerometer position.
     */
    @NonNull
    private final SensorManager mSensorManager;

    /**
     * Used to get the accelerometer position.
     */
    @NonNull
    private final Sensor mSensor;

    /**
     * Used to listen the accelerometer position.
     */
    @NonNull
    private final SensorEventListener mSensorEventListener;

    /**
     * Listen the value given by the accelerometer.
     */
    @NonNull
    private final List<OnAccelerometerValueListener> mOnAccelerometerValueListeners = new ArrayList<>();

    @NonNull
    private final AccelerometerSmooth mAccelerometerSmooth;
    // endregion Private, non null and final fields.

    /**
     * Singleton constructor.
     */
    private AccelerometerManager(@NonNull final Context context) {
        mAccelerometerSmooth = new AccelerometerSmooth();
        mSensorManager = (SensorManager) context.getSystemService(Context.SENSOR_SERVICE);
        mSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        mSensorEventListener = new SensorEventListener() {
            @Override
            public void onSensorChanged(final SensorEvent event) {
                final float axisX = -event.values[0];
                final float value;
                if (axisX > ACCELEROMETER_X_RANGE) {
                    value = 1;
                } else if (axisX < -ACCELEROMETER_X_RANGE) {
                    value = 0;
                } else {
                    value = (axisX + ACCELEROMETER_X_RANGE) / (2f * ACCELEROMETER_X_RANGE);
                }
                notifyNewPosition(value, mAccelerometerSmooth.smoothValues(value));
            }

            @Override
            public void onAccuracyChanged(final Sensor sensor, final int accuracy) {

            }
        };
    }

    public void addOnAccelerometerValueListener(
            @NonNull final OnAccelerometerValueListener onAccelerometerValueListener) {
        if (!mOnAccelerometerValueListeners.contains(onAccelerometerValueListener)) {
            mOnAccelerometerValueListeners.add(onAccelerometerValueListener);
        }
        if (!mIsStarted) {
            start();
        }
    }

    public void removeOnAccelerometerValueListener(
            @NonNull final OnAccelerometerValueListener onAccelerometerValueListener) {
        mOnAccelerometerValueListeners.remove(onAccelerometerValueListener);
        if (mOnAccelerometerValueListeners.isEmpty()) {
            stop();
        }
    }

    public void removeAllOnAccelerometerValueListener() {
        mOnAccelerometerValueListeners.clear();
        stop();
    }

    public void start() {
        if (!mIsStarted) {
            mIsStarted = true;
            mSensorManager.registerListener(mSensorEventListener, mSensor, SensorManager.SENSOR_DELAY_GAME);
        }
    }

    public void stop() {
        if (mIsStarted) {
            mIsStarted = false;
            mSensorManager.unregisterListener(mSensorEventListener, mSensor);
        }
    }

    private void notifyNewPosition(
            @FloatRange(from = 0.0, to = 1.0) final float value,
            @FloatRange(from = 0.0, to = 1.0) final float smoothValue) {
        for (final OnAccelerometerValueListener onAccelerometerValueListener : mOnAccelerometerValueListeners) {
            onAccelerometerValueListener.onAccelerometerValueChanged(value, smoothValue);
        }
    }

    public interface OnAccelerometerValueListener {

        /**
         * The accelerometer value changed.
         *
         * @param value The new value.
         */
        void onAccelerometerValueChanged(
                @FloatRange(from = 0.0, to = 1.0) final float value,
                @FloatRange(from = 0.0, to = 1.0) final float smoothValue);
    }
}
