package com.mercandalli.android.apps.raspberry.gas;

import android.support.annotation.FloatRange;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.mercandalli.android.library.base.main.UiManager;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

public class GasButtonManager extends UiManager {

    /**
     * Singleton instance.
     */
    @Nullable
    private static GasButtonManager sInstance;

    /**
     * Get the singleton instance.
     */
    @NonNull
    public static GasButtonManager getInstance() {
        if (sInstance == null) {
            sInstance = new GasButtonManager();
        }
        return sInstance;
    }

    @NonNull
    private final List<OnButtonPressedChangeListener> mOnButtonPressedChangeListeners
            = new ArrayList<>();

    private GasButtonManager() {

    }

    /* package */ void onGasButtonDown(
            @FloatRange(from = 0.0, to = 1.0) final float gasPercent) {
        notifyOnGasButtonDown(gasPercent);
    }

    /* package */ void onGasButtonMove(
            @FloatRange(from = 0.0, to = 1.0) final float gasPercent) {
        notifyOnGasButtonMove(gasPercent);
    }

    /* package */ void onGasButtonUp(
            @FloatRange(from = 0.0, to = 1.0) final float gasPercent) {
        notifyOnGasButtonUp(gasPercent);
    }

    public boolean addOnButtonPressedChangeListener(
            final OnButtonPressedChangeListener onButtonPressedChangeListener) {
        synchronized (mOnButtonPressedChangeListeners) {
            //noinspection SimplifiableIfStatement
            if (onButtonPressedChangeListener == null ||
                    mOnButtonPressedChangeListeners.contains(onButtonPressedChangeListener)) {
                // We don't allow to register null listener
                // And a listener can only be added once.
                return false;
            }
            return mOnButtonPressedChangeListeners.add(onButtonPressedChangeListener);
        }
    }

    public boolean removeOnEventsLoadingListener(
            final OnButtonPressedChangeListener onButtonPressedChangeListener) {
        synchronized (mOnButtonPressedChangeListeners) {
            return mOnButtonPressedChangeListeners.remove(onButtonPressedChangeListener);
        }
    }

    private void notifyOnGasButtonDown(
            @FloatRange(from = 0.0, to = 1.0) final float gasPercent) {
        if (Thread.currentThread() != mUiThread) {
            mUiHandler.post(new Runnable() {
                @Override
                public void run() {
                    notifyOnGasButtonDown(gasPercent);
                }
            });
            return;
        }
        synchronized (mOnButtonPressedChangeListeners) {
            final ListIterator<OnButtonPressedChangeListener> onButtonPressedChangeListenerListIterator
                    = mOnButtonPressedChangeListeners.listIterator();
            while (onButtonPressedChangeListenerListIterator.hasNext()) {
                final OnButtonPressedChangeListener onButtonPressedChangeListener =
                        onButtonPressedChangeListenerListIterator.next();
                if (onButtonPressedChangeListener.onGasButtonDown(gasPercent)) {
                    onButtonPressedChangeListenerListIterator.remove();
                }
            }
        }
    }

    private void notifyOnGasButtonMove(
            @FloatRange(from = 0.0, to = 1.0) final float gasPercent) {
        if (Thread.currentThread() != mUiThread) {
            mUiHandler.post(new Runnable() {
                @Override
                public void run() {
                    notifyOnGasButtonMove(gasPercent);
                }
            });
            return;
        }
        synchronized (mOnButtonPressedChangeListeners) {
            final ListIterator<OnButtonPressedChangeListener> onButtonPressedChangeListenerListIterator
                    = mOnButtonPressedChangeListeners.listIterator();
            while (onButtonPressedChangeListenerListIterator.hasNext()) {
                final OnButtonPressedChangeListener onButtonPressedChangeListener =
                        onButtonPressedChangeListenerListIterator.next();
                if (onButtonPressedChangeListener.onGasButtonMove(gasPercent)) {
                    onButtonPressedChangeListenerListIterator.remove();
                }
            }
        }
    }

    private void notifyOnGasButtonUp(
            @FloatRange(from = 0.0, to = 1.0) final float gasPercent) {
        if (Thread.currentThread() != mUiThread) {
            mUiHandler.post(new Runnable() {
                @Override
                public void run() {
                    onGasButtonUp(gasPercent);
                }
            });
            return;
        }
        synchronized (mOnButtonPressedChangeListeners) {
            final ListIterator<OnButtonPressedChangeListener> onButtonPressedChangeListenerListIterator
                    = mOnButtonPressedChangeListeners.listIterator();
            while (onButtonPressedChangeListenerListIterator.hasNext()) {
                final OnButtonPressedChangeListener onButtonPressedChangeListener =
                        onButtonPressedChangeListenerListIterator.next();
                if (onButtonPressedChangeListener.onGasButtonUp(gasPercent)) {
                    onButtonPressedChangeListenerListIterator.remove();
                }
            }

        }
    }

    public interface OnButtonPressedChangeListener {

        /**
         * @param gasPercent
         * @return <code>true</code> if the caller has to remove the listener, otherwise keep this
         * listener in the {@link List} of listeners.
         */
        boolean onGasButtonDown(
                @FloatRange(from = 0.0, to = 1.0) final float gasPercent);

        /**
         * @param gasPercent
         * @return <code>true</code> if the caller has to remove the listener, otherwise keep this
         * listener in the {@link List} of listeners.
         */
        boolean onGasButtonMove(
                @FloatRange(from = 0.0, to = 1.0) final float gasPercent);

        /**
         * @param gasPercent
         * @return <code>true</code> if the caller has to remove the listener, otherwise keep this
         * listener in the {@link List} of listeners.
         */
        boolean onGasButtonUp(
                @FloatRange(from = 0.0, to = 1.0) final float gasPercent);
    }
}
