package com.mercandalli.android.apps.raspberry.gas;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.support.annotation.FloatRange;
import android.support.annotation.NonNull;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import com.mercandalli.android.library.base.view.ViewUtils;

import static com.mercandalli.android.library.base.java.MathUtils.forceBetween;
import static com.mercandalli.android.library.base.view.ViewUtils.dpToPx;

public class GasButton extends View {

    private static final float HALF_PERCENT_MAGNET = 0.01f;

    private int mWidth;

    private int mHeight;

    @NonNull
    private final Paint mPaint = new Paint();

    @NonNull
    private final Paint mPressedPaint = new Paint();

    @NonNull
    private final Paint mLinePaint = new Paint();

    @NonNull
    private final Paint mLineTextPaint = new Paint();

    private int mLineTextHeight;

    @NonNull
    private final Paint mSmallLineTextPaint = new Paint();

    private boolean mIsPressed = false;

    @FloatRange(from = 0.0, to = 1.0)
    private float mGasPercent;

    private float mHalfLineHeight;

    private float mHalfSmallLineHeight;

    @NonNull
    private final GasButtonManager mGasButtonManager = GasButtonManager.getInstance();

    public GasButton(final Context context) {
        super(context);
        init(context);
    }

    public GasButton(final Context context, final AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public GasButton(final Context context, final AttributeSet attrs, final int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    private void init(final Context context) {
        if (isInEditMode()) {
            return;
        }
        mPaint.setColor(Color.parseColor("#505050"));
        mPressedPaint.setColor(Color.parseColor("#707070"));
        mLinePaint.setColor(Color.parseColor("#ffffff"));
        mLineTextPaint.setColor(Color.WHITE);
        mSmallLineTextPaint.setColor(Color.RED);
        mLineTextPaint.setTextSize(ViewUtils.spToPx(12));
        final Rect bounds = new Rect();
        mLineTextPaint.getTextBounds("0.00 %", 0, 4, bounds);
        mLineTextHeight = bounds.height();
    }

    @Override
    protected void onMeasure(final int widthMeasureSpec, final int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);

        mHeight = getMeasuredHeight();
        mWidth = getMeasuredWidth();
        mGasPercent = 0.5f;
        mHalfLineHeight = mHeight / 40;
        mHalfSmallLineHeight = dpToPx(0.4f);
        invalidate();
    }

    @Override
    protected void onDraw(final Canvas canvas) {
        super.onDraw(canvas);
        canvas.drawRect(0, 0, mWidth, mHeight, mIsPressed ? mPressedPaint : mPaint);

        final float y = mGasPercent * mHeight;
        canvas.drawRect(0, mHeight - (y - mHalfLineHeight),
                mWidth, mHeight - (y + mHalfLineHeight), mLinePaint);
        canvas.drawRect(0, mHeight - (y - mHalfSmallLineHeight),
                mWidth, mHeight - (y + mHalfSmallLineHeight), mSmallLineTextPaint);

        canvas.drawText(formatValue(mGasPercent) + " %", 20, (y > mHeight - mLineTextHeight - 60) ?
                mHeight - (y - mHalfLineHeight) + mLineTextHeight + 30 // Bellow
                : mHeight - (y - mHalfLineHeight) - 80, mLineTextPaint);
    }

    @Override
    public boolean onTouchEvent(final MotionEvent event) {
        final float y = event.getY();
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                mIsPressed = true;
                mGasPercent = magnet(forceBetween(1 - y / mHeight, 0, 1));
                invalidate();
                mGasButtonManager.onGasButtonDown(mGasPercent);
                break;
            case MotionEvent.ACTION_MOVE:
                mIsPressed = true;
                mGasPercent = magnet(forceBetween(1 - y / mHeight, 0, 1));
                invalidate();
                mGasButtonManager.onGasButtonMove(mGasPercent);
                break;
            case MotionEvent.ACTION_UP:
                mIsPressed = false;
                mGasPercent = 0.5f;
                invalidate();
                mGasButtonManager.onGasButtonUp(mGasPercent);
                break;
        }
        return true;
    }

    private String formatValue(final float value) {
        final float roundFloat = roundFloat(value);
        final String valueToDisplay = String.valueOf(roundFloat);
        if (((int) (roundFloat * 100)) % 10 == 0) {
            return valueToDisplay + "0";
        }
        return valueToDisplay;
    }

    private float roundFloat(final float value) {
        return ((float) Math.round(100 * value)) / 100f;
    }

    private static float magnet(final float value) {
        if (0.5f + HALF_PERCENT_MAGNET > value && value > 0.5f - HALF_PERCENT_MAGNET) {
            return 0.5f;
        }
        return value;
    }
}
