package com.mercandalli.android.apps.raspberry;

import android.support.multidex.MultiDexApplication;

import com.mercandalli.android.library.base.main.BaseManager;

public class RaspberryApp extends MultiDexApplication {

    @Override
    public void onCreate() {
        super.onCreate();
        BaseManager.getInstance().initialize(this, "1024356871075");
        EngineManager.getInstance().initialize();
    }
}
