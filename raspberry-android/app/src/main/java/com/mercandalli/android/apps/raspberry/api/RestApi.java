package com.mercandalli.android.apps.raspberry.api;

/**
 * Static urls.
 */
public final class RestApi {

    public static final String URL_DOMAIN = "http://mercandalli.com/";

    public static final String NODE_PROJECT = "/raspberry/raspberry-android/api";
}
