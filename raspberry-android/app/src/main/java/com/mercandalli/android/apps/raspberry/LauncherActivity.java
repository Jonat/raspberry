package com.mercandalli.android.apps.raspberry;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.FloatRange;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.TextView;

import com.mercandalli.android.apps.raspberry.accelerometer.AccelerometerManager;
import com.mercandalli.android.apps.raspberry.api.PhpApiManager;
import com.mercandalli.java.shared.model.Hardware;

import static com.mercandalli.android.library.base.view.ViewUtils.setTextViewText;

public class LauncherActivity extends Activity implements
        AccelerometerManager.OnAccelerometerValueListener,
        View.OnClickListener {

    @Nullable
    private TextView mDirectionTextView;

    @Nullable
    private TextView mLED1TextView;

    /**
     * {@inheritDoc}
     */
    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.launcher_activity);
        mDirectionTextView = (TextView) findViewById(R.id.launcher_activity_main_text_view);
        mLED1TextView = (TextView) findViewById(R.id.launcher_activity_led_1);
        AccelerometerManager.getInstance(this).addOnAccelerometerValueListener(this);
        findViewById(R.id.launcher_activity_led_1).setOnClickListener(this);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void onDestroy() {
        AccelerometerManager.getInstance(this).removeOnAccelerometerValueListener(this);
        super.onDestroy();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onAccelerometerValueChanged(
            @FloatRange(from = 0.0, to = 1.0) final float value,
            @FloatRange(from = 0.0, to = 1.0) final float smoothValue) {
        String valueToDisplay = String.valueOf(smoothValue);
        if (((int) (smoothValue * 100)) % 10 == 0) {
            valueToDisplay += "0";
        }
        setTextViewText(mDirectionTextView, valueToDisplay + " %");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onClick(final View v) {
        final int id = v.getId();
        switch (id) {
            case R.id.launcher_activity_led_1:
                if ("SEND LED ON".equals(mLED1TextView.getText().toString())) {
                    PhpApiManager.getInstance().writeLed(Hardware.ID_LED_1, true);
                    mLED1TextView.setText("SEND LED OFF");
                } else {
                    PhpApiManager.getInstance().writeLed(Hardware.ID_LED_1, false);
                    mLED1TextView.setText("SEND LED ON");
                }
                break;
        }
    }
}