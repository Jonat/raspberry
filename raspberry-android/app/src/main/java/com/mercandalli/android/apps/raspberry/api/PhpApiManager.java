package com.mercandalli.android.apps.raspberry.api;

import android.support.annotation.FloatRange;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.mercandalli.android.library.base.main.UiManager;
import com.mercandalli.java.shared.api.NucCommandApi;
import com.mercandalli.java.shared.model.Hardware;
import com.mercandalli.java.shared.model.PhpCommand;
import com.mercandalli.java.shared.model.RaspberryCommand;
import com.mercandalli.java.shared.utils.CreateCommandUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PhpApiManager extends UiManager implements NucCommandApi {

    @Nullable
    private static PhpApiManager sInstance;

    @NonNull
    public static PhpApiManager getInstance() {
        if (sInstance == null) {
            sInstance = new PhpApiManager();
        }
        return sInstance;
    }

    @NonNull
    private final OnlineApi mOnlineApi;

    @NonNull
    private final List<OnCommandReceivedListener> mOnCommandReceivedListeners = new ArrayList<>();

    private PhpApiManager() {
        mOnlineApi = RetrofitUtils.getRetrofitEncoded().create(OnlineApi.class);
    }

    @Override
    public void sendCommand(@NonNull final PhpCommand command) {
        mOnlineApi.sendCommand(command)
                .enqueue(new Callback<RaspberryCommand>() {
                    @Override
                    public void onResponse(
                            final Call<RaspberryCommand> call,
                            final Response<RaspberryCommand> response) {
                        final RaspberryCommand raspberryCommand;
                        if (response == null || !response.isSuccessful() ||
                                (raspberryCommand = response.body()) == null ||
                                !raspberryCommand.isSucceeded()) {
                            notifyOnCommandReceivedFailed();
                            return;
                        }
                        notifyOnCommandReceivedSucceeded(raspberryCommand);
                    }

                    @Override
                    public void onFailure(
                            final Call<RaspberryCommand> call,
                            final Throwable t) {
                        notifyOnCommandReceivedFailed();
                    }
                });
    }

    @Override
    public void sendCommand(
            @Hardware.HardwareLedId final int id,
            @NonNull final PhpCommand command) {
        sendCommand(command);
    }

    @Override
    public void writeLed(
            @Hardware.HardwareLedId final int id,
            final boolean on) {
        sendCommand(CreateCommandUtils.createWriteLedCommand(id, on));
    }

    @Override
    public void writeServo(
            @Hardware.HardwareServoId final int id,
            @FloatRange(from = 0.0, to = 1.0) final float value) {
        sendCommand(CreateCommandUtils.createWriteServoCommand(id, value));
    }

    public boolean addOnCommandReceivedListener(
            @Nullable final OnCommandReceivedListener onCommandReceivedListener) {
        synchronized (mOnCommandReceivedListeners) {
            //noinspection SimplifiableIfStatement
            if (onCommandReceivedListener == null ||
                    mOnCommandReceivedListeners.contains(onCommandReceivedListener)) {
                // We don't allow to register null listener
                // And a listener can only be added once.
                return false;
            }
            return mOnCommandReceivedListeners.add(onCommandReceivedListener);
        }
    }

    public boolean removeOnCommandReceivedListener(
            final OnCommandReceivedListener onCommandReceivedListener) {
        synchronized (mOnCommandReceivedListeners) {
            return mOnCommandReceivedListeners.remove(onCommandReceivedListener);
        }
    }

    private void notifyOnCommandReceivedSucceeded(@NonNull final RaspberryCommand command) {
        if (Thread.currentThread() != mUiThread) {
            mUiHandler.post(new Runnable() {
                @Override
                public void run() {
                    notifyOnCommandReceivedSucceeded(command);
                }
            });
            return;
        }
        synchronized (mOnCommandReceivedListeners) {
            final ListIterator<OnCommandReceivedListener> onCommandReceivedListenerListIterator =
                    mOnCommandReceivedListeners.listIterator();
            while (onCommandReceivedListenerListIterator.hasNext()) {
                final OnCommandReceivedListener onCommandReceivedListener =
                        onCommandReceivedListenerListIterator.next();
                if (onCommandReceivedListener.onCommandReceivedSucceeded(command)) {
                    onCommandReceivedListenerListIterator.remove();
                }
            }
        }
    }

    private void notifyOnCommandReceivedFailed() {
        if (Thread.currentThread() != mUiThread) {
            mUiHandler.post(new Runnable() {
                @Override
                public void run() {
                    notifyOnCommandReceivedFailed();
                }
            });
            return;
        }
        synchronized (mOnCommandReceivedListeners) {
            final ListIterator<OnCommandReceivedListener> onCommandReceivedListenerListIterator =
                    mOnCommandReceivedListeners.listIterator();
            while (onCommandReceivedListenerListIterator.hasNext()) {
                final OnCommandReceivedListener onCommandReceivedListener =
                        onCommandReceivedListenerListIterator.next();
                if (onCommandReceivedListener.onCommandReceivedFailed()) {
                    onCommandReceivedListenerListIterator.remove();
                }
            }
        }
    }

    public interface OnCommandReceivedListener {

        /**
         * @param command The {@link RaspberryCommand} from the server.
         * @return <code>true</code> if the caller has to remove the listener, otherwise keep this
         * listener in the {@link List} of listeners.
         */
        boolean onCommandReceivedSucceeded(@NonNull final RaspberryCommand command);

        /**
         * @return <code>true</code> if the caller has to remove the listener, otherwise keep this
         * listener in the {@link List} of listeners.
         */
        boolean onCommandReceivedFailed();
    }
}
