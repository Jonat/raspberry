package com.mercandalli.android.apps.raspberry.api;

import com.mercandalli.java.shared.model.PhpCommand;
import com.mercandalli.java.shared.model.RaspberryCommand;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

/* package */ interface OnlineApi {

    @POST(RestApi.NODE_PROJECT + "/robotics")
    Call<RaspberryCommand> sendCommand(@Body final PhpCommand requestResponse);
}
