package com.mercandalli.android.apps.raspberry.accelerometer;

import android.support.annotation.FloatRange;

/* package */ class AccelerometerSmooth {

    private float mLastLastValue = -1f;
    private float mLastValue = -1f;

    /* package */ AccelerometerSmooth() {
    }

    @FloatRange(from = 0.0, to = 1.0)
    /* package */ float smoothValues(@FloatRange(from = 0.0, to = 1.0) final float brutValue) {
        final float roundedValue = roundFloat(brutValue);
        final float outPutValue;
        if (mLastValue == -1f && mLastLastValue == -1f) {
            outPutValue = roundedValue;
        } else if (mLastLastValue == -1f) {
            outPutValue = roundFloat((4f * roundedValue + 9f * mLastValue) / 13f);
        } else {
            outPutValue = roundFloat(
                    (4f * roundedValue + 5f * mLastValue + 6f * mLastLastValue) / 15f);
        }
        mLastLastValue = mLastValue;
        mLastValue = outPutValue;
        return outPutValue;
    }

    private float roundFloat(final float value) {
        return ((float) Math.round(100 * value)) / 100f;
    }
}
