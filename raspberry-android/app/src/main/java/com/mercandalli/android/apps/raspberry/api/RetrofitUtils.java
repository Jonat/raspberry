package com.mercandalli.android.apps.raspberry.api;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import com.mercandalli.android.apps.raspberry.BuildConfig;
import com.mercandalli.java.shared.utils.CryptUtils;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import okio.Buffer;
import okio.BufferedSink;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.google.android.gms.internal.zzs.TAG;

/* package */ final class RetrofitUtils {

    private RetrofitUtils() {
        // Non-instantiable.
    }

    @NonNull
    private static OkHttpClient getOkHttpClientEncoded() {
        final OkHttpClient.Builder builder = (new OkHttpClient.Builder())
                .connectTimeout(60, TimeUnit.SECONDS)
                .readTimeout(60, TimeUnit.SECONDS);
        builder.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(final Chain chain) throws IOException {
                Request originalRequest = chain.request().newBuilder()
                        .build();
                Request.Builder builder = originalRequest.newBuilder();

                //detect if it's a POST request
                if (originalRequest.method().equalsIgnoreCase("POST")) {
                    // encode JSON intro base
                    builder = originalRequest.newBuilder().method(
                            originalRequest.method(),
                            encode(originalRequest.body()));
                }
                return chain.proceed(builder.build());
            }

            /**
             * Encode JSON into Base64.
             * @param body A {@link RequestBody} containing JSON.
             * @return A {@link RequestBody} with content converted into Base64.
             */
            private RequestBody encode(final RequestBody body) {
                return new RequestBody() {
                    @Override
                    public MediaType contentType() {
                        return body.contentType();
                    }

                    @Override
                    public void writeTo(final BufferedSink sink) throws IOException {
                        final Buffer buffer = new Buffer();
                        body.writeTo(buffer);
                        final String clear = buffer.readUtf8();
                        final String crypt = encrypt(clear);
                        sink.write(crypt.getBytes("UTF-8"));
                        buffer.close();
                        sink.close();
                        if (BuildConfig.DEBUG) {
                            Log.d(TAG, "Clear: " + clear);
                            if (!clear.equals(decrypt(crypt))) {
                                Log.e(TAG, "Crypt failed");
                            } else {
                                Log.d(TAG, "Crypt succeeded");
                            }
                        }
                    }
                };
            }
        });
        if (BuildConfig.DEBUG) {
            final HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            builder.addInterceptor(interceptor);
        }
        return builder.build();
    }

    @NonNull
    private static OkHttpClient getOkHttpClient() {
        final OkHttpClient.Builder builder = (new OkHttpClient.Builder())
                .connectTimeout(60, TimeUnit.SECONDS)
                .readTimeout(60, TimeUnit.SECONDS);
        if (BuildConfig.DEBUG) {
            final HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            builder.addInterceptor(interceptor);
        }
        return builder.build();
    }

    @NonNull
    private static OkHttpClient getAuthorizedOkHttpClient() {
        final OkHttpClient.Builder builder = (new OkHttpClient.Builder())
                .connectTimeout(60, TimeUnit.SECONDS)
                .readTimeout(60, TimeUnit.SECONDS)
                .addInterceptor(new Interceptor() {
                    @Override
                    public Response intercept(Chain chain) throws IOException {
                        Request original = chain.request();

                        // Customize the request
                        Request request = original.newBuilder()
                                .header("Accept", "application/json")
                                //.header("Authorization", "Basic " + "TODO")
                                .method(original.method(), original.body())
                                .build();

                        // Customize or return the response
                        return chain.proceed(request);
                    }
                });
        if (BuildConfig.DEBUG) {
            final HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            builder.addInterceptor(interceptor);
        }
        return builder.build();
    }

    @NonNull
    public static Retrofit getRetrofit() {
        return (new Retrofit.Builder())
                .baseUrl(RestApi.URL_DOMAIN)
                .addConverterFactory(GsonConverterFactory.create())
                .client(getOkHttpClient())
                .build();
    }

    @NonNull
    public static Retrofit getRetrofitEncoded() {
        return (new Retrofit.Builder())
                .baseUrl(RestApi.URL_DOMAIN)
                .addConverterFactory(GsonConverterFactory.create())
                .client(getOkHttpClientEncoded())
                .build();
    }

    @NonNull
    public static Retrofit getAuthorizedRetrofit() {
        return (new Retrofit.Builder())
                .baseUrl(RestApi.URL_DOMAIN)
                .addConverterFactory(GsonConverterFactory.create())
                .client(getAuthorizedOkHttpClient())
                .build();
    }

    @NonNull
    private static String encrypt(@Nullable final String clear) {
        return CryptUtils.encryptCustom(
                clear,
                BuildConfig.PHP_API_CRYPT_A_GAIN,
                BuildConfig.PHP_API_CRYPT_B_OFFSET,
                BuildConfig.PHP_API_CRYPT_C_MOD);
    }

    @NonNull
    private static String decrypt(@Nullable final String clear) {
        return CryptUtils.decryptCustom(
                clear,
                BuildConfig.PHP_API_CRYPT_A_GAIN,
                BuildConfig.PHP_API_CRYPT_B_OFFSET,
                BuildConfig.PHP_API_CRYPT_C_MOD);
    }
}
