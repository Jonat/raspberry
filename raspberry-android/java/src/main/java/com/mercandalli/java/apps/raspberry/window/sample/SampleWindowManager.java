package com.mercandalli.java.apps.raspberry.window.sample;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

/**
 * If you want a rapid window to display results.
 */
public class SampleWindowManager {

    @Nullable
    private static SampleWindowManager sInstance;

    @NonNull
    public static SampleWindowManager getInstance() {
        if (sInstance == null) {
            sInstance = new SampleWindowManager();
        }
        return sInstance;
    }

    @NonNull
    private final SampleWindow mTestWindow;

    private SampleWindowManager() {
        mTestWindow = new SampleWindow();
    }

    public void show() {
        mTestWindow.setVisible(true);
    }
}
