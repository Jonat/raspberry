package com.mercandalli.java.apps.raspberry.network;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.mercandalli.java.apps.raspberry.log.Log;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

import javax.net.ssl.HttpsURLConnection;

/**
 * If you want a rapid window to display results.
 */
public class NetworkManager {

    private static final String TAG = "NetworkManager";

    @Nullable
    private static NetworkManager sInstance;

    @NonNull
    public static NetworkManager getInstance() {
        if (sInstance == null) {
            sInstance = new NetworkManager();
        }
        return sInstance;
    }

    /**
     * The main (UI) {@link Thread} used to be sure that the callbacks are on the main {@link Thread}.
     */
    @NonNull
    private final Thread mUiThread = Thread.currentThread();

    @NonNull
    private final List<OnGetListener> mOnGetListeners = new ArrayList<>();

    @NonNull
    private final List<OnPostListener> mOnPostListeners = new ArrayList<>();

    private NetworkManager() {

    }

    public void httpGet(@NonNull final String httpUrl) {
        new Thread(() -> {
            try {
                final URL url = new URL(httpUrl);
                String query = "";

                //make connection
                final URLConnection urlc = url.openConnection();

                //use post mode
                urlc.setDoOutput(true);
                urlc.setAllowUserInteraction(false);

                //send query
                final PrintStream ps = new PrintStream(urlc.getOutputStream());
                ps.print(query);
                ps.close();

                //get result
                BufferedReader br = new BufferedReader(new InputStreamReader(urlc
                        .getInputStream()));
                String l = null;
                while ((l = br.readLine()) != null) {
                    System.out.println(l);
                }
                br.close();
                notifyOnGetSucceeded();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }).start();
    }


    public void httpPost(@NonNull final String httpsURL, @Nullable final String queryJson) {
        httpPost(-1, httpsURL, queryJson);
    }

    public void httpPost(final int requestId, @NonNull final String httpsURL, @Nullable final String queryJson) {
        new Thread(() -> {
            try {
                final URL url = new URL(httpsURL);
                final HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                httpURLConnection.setRequestMethod("POST");

                if (queryJson != null) {
                    httpURLConnection.setRequestProperty("Content-length", String.valueOf(queryJson.length()));
                }
                httpURLConnection.setRequestProperty("Content-Type", "application/json");
                httpURLConnection.setRequestProperty("User-Agent", "Mozilla/4.0 (compatible; MSIE 5.0;Windows98;DigExt)");
                httpURLConnection.setDoOutput(true);
                httpURLConnection.setDoInput(true);
                httpURLConnection.setConnectTimeout(4_000);

                if (queryJson != null) {
                    final DataOutputStream output = new DataOutputStream(httpURLConnection.getOutputStream());
                    output.writeBytes(queryJson);
                    output.close();
                }

                final StringBuilder responseStringBuilder = new StringBuilder();
                final DataInputStream input = new DataInputStream(httpURLConnection.getInputStream());
                for (int c = input.read(); c != -1; c = input.read()) {
                    responseStringBuilder.append((char) c);
                }
                input.close();

                log("httpPost", "Response:" + responseStringBuilder.toString());
                notifyOnPostSucceeded(requestId, responseStringBuilder.toString());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }).start();
    }

    public void httpsGet(@NonNull final String httpsURL) {
        new Thread(() -> {
            try {
                final URL url = new URL(httpsURL);
                final HttpsURLConnection httpsURLConnection = (HttpsURLConnection) url.openConnection();
                httpsURLConnection.setRequestMethod("GET");

                httpsURLConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                httpsURLConnection.setRequestProperty("User-Agent", "Mozilla/4.0 (compatible; MSIE 5.0;Windows98;DigExt)");
                httpsURLConnection.setDoOutput(true);
                httpsURLConnection.setDoInput(true);

                final DataInputStream input = new DataInputStream(httpsURLConnection.getInputStream());

                for (int c = input.read(); c != -1; c = input.read()) {
                    System.out.print((char) c);
                }
                input.close();

                log("httpsGet", "Resp Code:" + httpsURLConnection.getResponseCode());
                log("httpsGet", "Resp Message:" + httpsURLConnection.getResponseMessage());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }).start();
    }

    public void httpsPost(@NonNull final String httpsURL, @Nullable final String query) {
        new Thread(() -> {
            try {
                final URL url = new URL(httpsURL);
                HttpsURLConnection httpsURLConnection = (HttpsURLConnection) url.openConnection();
                httpsURLConnection.setRequestMethod("POST");

                if (query != null) {
                    httpsURLConnection.setRequestProperty("Content-length", String.valueOf(query.length()));
                }
                httpsURLConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                httpsURLConnection.setRequestProperty("User-Agent", "Mozilla/4.0 (compatible; MSIE 5.0;Windows98;DigExt)");
                httpsURLConnection.setDoOutput(true);
                httpsURLConnection.setDoInput(true);
                httpsURLConnection.setConnectTimeout(4_000);

                if (query != null) {
                    final DataOutputStream output = new DataOutputStream(httpsURLConnection.getOutputStream());
                    output.writeBytes(URLEncoder.encode(query, "UTF-8"));
                    output.close();
                }

                final DataInputStream input = new DataInputStream(httpsURLConnection.getInputStream());
                for (int c = input.read(); c != -1; c = input.read()) {
                    System.out.print((char) c);
                }
                input.close();

                System.out.println("Resp Code:" + httpsURLConnection.getResponseCode());
                System.out.println("Resp Message:" + httpsURLConnection.getResponseMessage());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }).start();
    }

    private void notifyOnGetSucceeded() {
        if (Thread.currentThread() != mUiThread) {

        }
    }

    public boolean addOnGetListener(@Nullable final OnGetListener onGetListener) {
        synchronized (mOnGetListeners) {
            //noinspection SimplifiableIfStatement
            if (onGetListener == null ||
                    mOnGetListeners.contains(onGetListener)) {
                // We don't allow to register null listener
                // And a listener can only be added once.
                return false;
            }
            return mOnGetListeners.add(onGetListener);
        }
    }

    public boolean removeOnGetListener(final OnGetListener onGetListener) {
        synchronized (mOnGetListeners) {
            return mOnGetListeners.remove(onGetListener);
        }
    }

    private void notifyOnPostSucceeded(final int requestId, @NonNull final String response) {
        if (Thread.currentThread() != mUiThread) {

        }
        final ListIterator<OnPostListener> onPostListenerListIterator = mOnPostListeners.listIterator();
        while (onPostListenerListIterator.hasNext()) {
            final OnPostListener next = onPostListenerListIterator.next();
            if (next.onPostSucceeded(requestId, response)) {
                onPostListenerListIterator.remove();
            }
        }
    }

    public boolean addOnPostListener(@Nullable final OnPostListener onPostListener) {
        synchronized (mOnPostListeners) {
            //noinspection SimplifiableIfStatement
            if (onPostListener == null ||
                    mOnPostListeners.contains(onPostListener)) {
                // We don't allow to register null listener
                // And a listener can only be added once.
                return false;
            }
            return mOnPostListeners.add(onPostListener);
        }
    }

    public boolean removeOnPostListener(final OnPostListener onPostListener) {
        synchronized (mOnPostListeners) {
            return mOnPostListeners.remove(onPostListener);
        }
    }

    private void log(@NonNull final String methodName, @NonNull final String message) {
        Log.d(TAG, "[" + methodName + "] " + message);
    }

    public interface OnGetListener {
        boolean onGetSucceeded(final int requestId, @NonNull final String response);

        boolean onGetFailed(final int requestId);
    }

    public interface OnPostListener {
        boolean onPostSucceeded(final int requestId, @NonNull final String response);

        boolean onPostFailed(final int requestId);
    }
}
