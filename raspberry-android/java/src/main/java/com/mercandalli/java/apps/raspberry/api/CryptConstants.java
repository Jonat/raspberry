package com.mercandalli.java.apps.raspberry.api;

import com.google.gson.annotations.SerializedName;

/* package */ class CryptConstants {

    @SerializedName("a_gain")
    /* package */ final int a;

    @SerializedName("b_offset")
    /* package */ final int b;

    @SerializedName("c_mod")
    /* package */ final int c ;

    /* package */ CryptConstants(final int a, final int b, final int c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }
}
