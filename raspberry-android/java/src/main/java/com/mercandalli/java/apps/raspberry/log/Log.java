package com.mercandalli.java.apps.raspberry.log;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import static com.mercandalli.java.apps.raspberry.utils.StringUtils.generateString;

public class Log {

    private static final int TAG_MAX_SIZE = 20;

    public static void d(@Nullable final String message) {
        System.out.println(String.valueOf(message));
    }

    public static void d(@NonNull final String tag, @Nullable final String message) {
        final int size = TAG_MAX_SIZE - tag.length();
        System.out.println("[" + tag + "]" + generateString(' ', size) + message);
    }
}
