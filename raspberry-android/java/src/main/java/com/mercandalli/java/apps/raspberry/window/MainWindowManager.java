package com.mercandalli.java.apps.raspberry.window;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.mercandalli.java.apps.raspberry.api.PhpApiManager;
import com.mercandalli.java.shared.model.Hardware;

/**
 * If you want a rapid window to display results.
 */
public class MainWindowManager implements
        PhpApiManager.OnWriteLedListener,
        PhpApiManager.OnWriteServoListener {

    @Nullable
    private static MainWindowManager sInstance;

    @NonNull
    public static MainWindowManager getInstance() {
        if (sInstance == null) {
            sInstance = new MainWindowManager();
        }
        return sInstance;
    }

    @NonNull
    private final PhpApiManager mApiManager = PhpApiManager.getInstance();

    @NonNull
    private final MainWindow mMainWindow = new MainWindow();

    private MainWindowManager() {
        mApiManager.addOnWriteLedListener(this);
        mApiManager.addOnWriteServoListener(this);
    }

    @Override
    public boolean onWriteLedSucceeded() {
        mMainWindow.setOutput("WriteLedSucceeded");
        return false;
    }

    @Override
    public boolean onWriteLedFailed(@NonNull final String errorMessage) {
        mMainWindow.setOutput("WriteLedFailed: " + errorMessage);
        return false;
    }

    @Override
    public boolean onWriteServoSucceeded() {
        mMainWindow.setOutput("WriteServoSucceeded");
        return false;
    }

    @Override
    public boolean onWriteServoFailed(@NonNull final String errorMessage) {
        mMainWindow.setOutput("WriteServoFailed: " + errorMessage);
        return false;
    }

    public void show() {
        mMainWindow.setVisible(true);
    }

    /* package */ void onLed1OnClick() {
        mApiManager.writeLed(Hardware.ID_LED_1, true);
    }

    /* package */ void onLed1OffClick() {
        mApiManager.writeLed(Hardware.ID_LED_1, false);
    }

    /* package */ void onServo1To0() {
        mApiManager.writeServo(Hardware.ID_SERVO_1_DIRECTION, 0);
    }

    /* package */ void onServo1To50() {
        mApiManager.writeServo(Hardware.ID_SERVO_1_DIRECTION, 0.5f);
    }

    /* package */ void onServo1To100() {
        mApiManager.writeServo(Hardware.ID_SERVO_1_DIRECTION, 1);
    }

    /* package */ void onServo2To0() {
        mApiManager.writeServo(Hardware.ID_SERVO_2_ENGINE, 0);
    }

    /* package */ void onServo2To50() {
        mApiManager.writeServo(Hardware.ID_SERVO_2_ENGINE, 0.5f);
    }

    /* package */ void onServo2To100() {
        mApiManager.writeServo(Hardware.ID_SERVO_2_ENGINE, 1);
    }
}
