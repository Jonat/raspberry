package com.mercandalli.java.apps.raspberry;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.mercandalli.java.apps.raspberry.log.Log;
import com.mercandalli.java.apps.raspberry.window.MainWindowManager;

/**
 * The launcher class.
 */
public class Main {

    public static final String PROGRAM_NAME = "Mercan-Raspberry";

    /**
     * The program input. The {@link com.mercandalli.java.apps.raspberry.window.MainWindow} call
     * {@link System#exit(int)} to close the program when the top-right window cross is clicked.
     *
     * @param args The program args.
     */
    public static void main(@NonNull final String[] args) {
        Log.d("Main", "The program " + PROGRAM_NAME + " start");
        MainWindowManager.getInstance().show();
    }

    public static void exit(@NonNull final String tag, @Nullable final String message) {
        Log.d(tag, message);
        Log.d(tag, "Close the jvm");
        System.exit(0);
    }
}
