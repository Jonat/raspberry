package com.mercandalli.java.apps.raspberry.api;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;
import com.mercandalli.java.apps.raspberry.Main;
import com.mercandalli.java.apps.raspberry.log.Log;
import com.mercandalli.java.shared.utils.CryptUtils;

import java.io.IOException;
import java.lang.reflect.Type;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Stream;

import javax.swing.JOptionPane;

/**
 * Crypt string to communicate wit the PhpApi.
 */
/* package */
class CryptManager {

    private static final String TAG = "CryptManager";

    private static final String FILE_AUTHENTICATION_LOCAL_PATH = "./config/crypt/crypt.json";
    private static final String FILE_AUTHENTICATION_LOCAL_PATH_2ND_TRY = "./crypt.json";

    @NonNull
    private static final Type TYPE_CRYPT_CONSTANTS = new TypeToken<CryptConstants>() {
    }.getType();

    @Nullable
    private static CryptManager sInstance;

    @NonNull
    /* package */ static CryptManager getInstance() {
        if (sInstance == null) {
            sInstance = new CryptManager();
        }
        return sInstance;
    }

    @Nullable
    private CryptConstants mCryptConstants;

    @SuppressWarnings("NewApi")
    private CryptManager() {
        final StringBuilder stringBuilder = new StringBuilder();
        try (Stream<String> stream = Files.lines(Paths.get(FILE_AUTHENTICATION_LOCAL_PATH))) {
            stream.forEach(stringBuilder::append);
            Log.d(TAG, "Find the key here: " + FILE_AUTHENTICATION_LOCAL_PATH);
        } catch (IOException e) {
            Log.d(TAG, "Can't find the file " + FILE_AUTHENTICATION_LOCAL_PATH + " so try on the app folder.");
            try (Stream<String> stream = Files.lines(Paths.get(FILE_AUTHENTICATION_LOCAL_PATH_2ND_TRY))) {
                stream.forEach(stringBuilder::append);
                Log.d(TAG, "Find the key in the app folder!");
            } catch (IOException e2) {
                Log.d(TAG, "Can't find the file " + FILE_AUTHENTICATION_LOCAL_PATH_2ND_TRY);
                Log.d(TAG, "So ask the user. ");
            }
        }
        while (stringBuilder.toString().isEmpty()) {
            stringBuilder.append(
                    JOptionPane.showInputDialog(
                            null,
                            "Enter the key:\n" +
                                    "   (If you don't want to type this key again,\n" +
                                    "   create a file crypt.json in the same folder\n" +
                                    "   of your app with the key inside)",
                            "key",
                            JOptionPane.PLAIN_MESSAGE));
        }
        try {
            mCryptConstants = new Gson().fromJson(stringBuilder.toString(), TYPE_CRYPT_CONSTANTS);
        } catch (JsonSyntaxException ignored) {
        }
        if (mCryptConstants == null) {
            Main.exit(TAG, "Can't create the key.");
        }
    }

    /* package */ String encrypt(@Nullable final String clear) {
        if (mCryptConstants == null) {
            Log.d(TAG, "encrypt failed because the key was wrong.");
            return "";
        }
        return CryptUtils.encryptCustom(clear, mCryptConstants.a, mCryptConstants.b, mCryptConstants.c);
    }
}
