package com.mercandalli.java.apps.raspberry.utils;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLDecoder;
import java.nio.charset.Charset;
import java.text.Normalizer;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Static methods for dealing with the {@link String}.
 */
@SuppressWarnings("unused")
public final class StringUtils {

    @NonNull
    private static final String TAG = "StringUtils";

    /**
     * Inspired by http://www.mkyong.com/regular-expressions/how-to-validate-email-address-with-regular-expression/
     */
    @NonNull
    public static final String EMAIL_PATTERN =
            "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                    + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

    private StringUtils() {
        // Non-instantiable.
    }

    /**
     * Returns true if the string is null or 0-length. Do not use Android "TextUtils" to test
     * on unit tests.
     *
     * @param str the string to be examined
     * @return true if str is null or zero length
     */
    public static boolean isEmpty(final @Nullable CharSequence str) {
        return str == null || str.length() == 0;
    }

    /**
     * Are two @Nullable {@link String}s equals. Accept null comparison.
     *
     * @param s1 The first {@link String} to compare.
     * @param s2 The second {@link String} to compare.
     * @return True if the {@link String}s are equal.
     */
    public static boolean isEquals(@Nullable final String s1, @Nullable final String s2) {
        return s1 == null && s2 == null || !(s1 == null || s2 == null) && s1.equals(s2);
    }

    /**
     * Split a string into words with punctuation split.
     *
     * @param string A {@link String} with space...
     * @return An array of words.
     */
    public static String[] splitWords(final String string) {
        return string.split("[[ ]*|[,]*|[\\.]*|[:]*|[/]*|[!]*|[?]*|[+]*]+");
    }

    public static String[] getWords(final String sentence) {
        if (sentence == null) {
            return null;
        }
        final StringTokenizer stToken = new StringTokenizer(sentence, " ");
        final int nbToken = stToken.countTokens();
        final String[] messageTab = new String[nbToken];
        int token = 0;
        while (stToken.hasMoreTokens()) {
            messageTab[token] = stToken.nextToken();
            token++;
        }
        return messageTab;
    }

    @Nullable
    public static String normalizeString(final String message) {
        if (message == null) {
            return null;
        }
        return replaceAccents(message.toLowerCase());
    }

    public static String replaceAccents(String message) {
        if (message == null) {
            return null;
        }
        message = Normalizer.normalize(message, Normalizer.Form.NFD);
        message = message.replaceAll("[\\p{InCombiningDiacriticalMarks}]", "");
        return message;
    }

    public static boolean isNullOrEmpty(final String str) {
        return str == null || str.replaceAll(" ", "").equals("");
    }

    public static String toEmptyIfNull(String str) {
        if (str == null) {
            return "";
        }
        return str;
    }

    public static String intToShortString(final int nb) {
        if (nb < 1_000) {
            return String.valueOf(nb);
        }
        int exp = (int) (Math.log(nb) / Math.log(1000));
        String pre = "" + ("KMGTPE").charAt(exp - 1);
        return String.format(Locale.getDefault(), "%.1f %s", nb / Math.pow(1000, exp), pre);
    }

    public static String longToShortString(long nb) {
        if (nb < 1000) {
            return String.valueOf(nb);
        }
        int exp = (int) (Math.log(nb) / Math.log(1000));
        String pre = "" + ("KMGTPE").charAt(exp - 1);
        return String.format(Locale.getDefault(), "%.1f %s", nb / Math.pow(1000, exp), pre);
    }

    public static String capitalize(final String str) {
        return capitalize(str, null);
    }

    public static String capitalize(final String str, final char... delimiters) {
        final int delimiterLen = delimiters == null ? -1 : delimiters.length;
        if (isNullOrEmpty(str) || delimiterLen == 0) {
            return str;
        }
        final char[] buffer = str.toCharArray();
        boolean capitalizeNext = true;
        for (int i = 0; i < buffer.length; i++) {
            final char ch = buffer[i];
            if (isDelimiter(ch, delimiters)) {
                capitalizeNext = true;
            } else if (capitalizeNext) {
                buffer[i] = Character.toTitleCase(ch);
                capitalizeNext = false;
            }
        }
        return new String(buffer);
    }

    public static String uppercase(@NonNull final String str) {
        return str.toUpperCase();
    }

    public static String substring(final String str, final int nb_first) {
        if (str == null) {
            return "";
        }
        return str.substring(0, nb_first);
    }

    private static boolean isDelimiter(final char ch, final char[] delimiters) {
        if (delimiters == null) {
            return Character.isWhitespace(ch);
        }
        for (final char delimiter : delimiters) {
            if (ch == delimiter) {
                return true;
            }
        }
        return false;
    }

    /**
     * Check if the email {@link String} is valid.
     *
     * @param email The email to check.
     * @return A boolean to know if the email is valid.
     */
    public static boolean isEmailValid(final String email) {
        if (email == null) {
            return false;
        }
        final Pattern emailPattern = Pattern.compile(EMAIL_PATTERN);
        final Matcher emailMatcher = emailPattern.matcher(email);
        return emailMatcher.matches();
    }

    @Nullable
    public static byte[] stringToByteArray(@NonNull final String str) {
        try {
            return str.getBytes("UTF-8");
        } catch (UnsupportedEncodingException ignored) {
        }
        return null;
    }

    @Nullable
    public static String byteArrayToString(@NonNull final byte[] bytes) {
        return new String(bytes, Charset.forName("UTF-8"));
    }

    @Nullable
    public static URL toUrl(@NonNull String inputUrl) {
        if (!inputUrl.contains("http://") && !inputUrl.contains("https://")) {
            inputUrl = "http://" + inputUrl;
        }
        try {
            return new URL(inputUrl);
        } catch (MalformedURLException ignored) {
        }
        return null;
    }

    @NonNull
    public static Map<String, String> getUrlParams(@NonNull String inputUrl) {
        try {
            final URL url = toUrl(URLDecoder.decode(inputUrl, "UTF-8"));
            if (url == null) {
                return new HashMap<>();
            }
            final Map<String, String> queryPairs = new LinkedHashMap<>();
            final String query = url.getQuery();
            final String[] pairs = query.split("&");
            for (String pair : pairs) {
                final int idx = pair.indexOf("=");
                queryPairs.put(URLDecoder.decode(pair.substring(0, idx), "UTF-8"),
                        URLDecoder.decode(pair.substring(idx + 1), "UTF-8"));
            }
            return queryPairs;
        } catch (UnsupportedEncodingException ignored) {
        }
        return new HashMap<>();
    }

    /**
     * Extract the parameters from the query {@link String}.
     *
     * @param query The {@link String} from the {@link android.content.BroadcastReceiver}.
     * @return The {@link Map} of parameters.
     */
    @NonNull
    public static Map<String, String> getParamsFromQuery(@NonNull final String query) {
        final String referrer;
        try {
            // Remove any url encoding
            referrer = URLDecoder.decode(query, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            return new HashMap<>();
        }
        if (referrer == null) {
            return new HashMap<>();
        }

        final Map<String, String> referralParams = new HashMap<>();
        // Parse the query string, extracting the relevant data
        final String[] params = referrer.split("&"); // $NON-NLS-1$
        for (final String param : params) {
            if (param != null) {
                final String[] pair = param.split("="); // $NON-NLS-1$
                if (pair.length == 2) {
                    referralParams.put(pair[0], pair[1]);
                }
            }
        }
        return referralParams;
    }

    /**
     * Here is pure java so don't use {@link android.text.TextUtils#join(CharSequence, Iterable)}.
     * <p>
     * (test purpose)
     */
    @NonNull
    public static String join(@Nullable final String text, @Nullable final Iterable list) {
        if (list == null) {
            return "";
        }
        final StringBuilder result = new StringBuilder();
        boolean notFirst = false;
        for (final Object next : list) {
            if (notFirst) {
                result.append(text);
            } else {
                notFirst = true;
            }
            result.append(next);
        }
        return result.toString();
    }

    public static String completeWithChar(
            @NonNull final String input,
            final char charToComplete,
            final int lengthOfTheOutput) {
        if (input.length() >= lengthOfTheOutput) {
            return input;
        }
        return input + generateString(charToComplete, lengthOfTheOutput - input.length());
    }

    public static String generateString(
            final char charToRepeat,
            final int times) {
        if (times < 1) {
            return "";
        }
        final StringBuilder output = new StringBuilder();
        for (int i = 0; i < times; i++) {
            output.append(charToRepeat);
        }
        return output.toString();
    }
}
