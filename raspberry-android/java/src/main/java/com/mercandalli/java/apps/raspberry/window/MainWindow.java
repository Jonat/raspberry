package com.mercandalli.java.apps.raspberry.window;

import android.support.annotation.NonNull;

import com.mercandalli.java.apps.raspberry.Main;

import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

/* package */ class MainWindow extends JFrame implements ActionListener {

    private static final String TAG = "MainWindow";

    private static final String BUTTON_LED_1_ON = "[LED-1] - ON";
    private static final String BUTTON_LED_1_OFF = "[LED-1] - OFF";
    private static final String BUTTON_SERVO_1_TO_0 = "[SERVO-1] - 0";
    private static final String BUTTON_SERVO_1_TO_50 = "[SERVO-1] - 50";
    private static final String BUTTON_SERVO_1_TO_100 = "[SERVO-1] - 100";
    private static final String BUTTON_SERVO_2_TO_0 = "[SERVO-2] - 0";
    private static final String BUTTON_SERVO_2_TO_50 = "[SERVO-2] - 50";
    private static final String BUTTON_SERVO_2_TO_100 = "[SERVO-2] - 100";

    @NonNull
    private final JLabel mJLabel;

    /* package */ MainWindow() {
        setTitle(Main.PROGRAM_NAME);

        final JPanel panel = new JPanel();
        panel.setBackground(new Color(33, 33, 33));
        panel.setLayout(new GridLayout(10, 1));
        getContentPane().add(panel);
        setSize(310, 400);

        addJButtonToPanel(panel, BUTTON_LED_1_ON);
        addJButtonToPanel(panel, BUTTON_LED_1_OFF);
        addJButtonToPanel(panel, BUTTON_SERVO_1_TO_0);
        addJButtonToPanel(panel, BUTTON_SERVO_1_TO_50);
        addJButtonToPanel(panel, BUTTON_SERVO_1_TO_100);
        addJButtonToPanel(panel, BUTTON_SERVO_2_TO_0);
        addJButtonToPanel(panel, BUTTON_SERVO_2_TO_50);
        addJButtonToPanel(panel, BUTTON_SERVO_2_TO_100);

        mJLabel = new JLabel();
        mJLabel.setForeground(Color.WHITE);
        final JLabel outPutLabel = new JLabel();
        outPutLabel.setText("    Output:");
        outPutLabel.setBackground(new Color(33, 33, 33));
        outPutLabel.setForeground(Color.WHITE);
        panel.add(outPutLabel);
        panel.add(mJLabel);

        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                e.getWindow().dispose();
                Main.exit(TAG, "Close clicked");
            }
        });
    }

    @Override
    public void actionPerformed(final ActionEvent e) {
        switch (e.getActionCommand()) {
            case BUTTON_LED_1_ON:
                setOutput("Working: led 1 on");
                MainWindowManager.getInstance().onLed1OnClick();
                invalidate();
                break;
            case BUTTON_LED_1_OFF:
                setOutput("Sending: led 1 off");
                MainWindowManager.getInstance().onLed1OffClick();
                invalidate();
                break;
            case BUTTON_SERVO_1_TO_0:
                setOutput("Sending: servo 1 to 0%");
                MainWindowManager.getInstance().onServo1To0();
                invalidate();
                break;
            case BUTTON_SERVO_1_TO_50:
                setOutput("Sending: servo 1 to 50%");
                MainWindowManager.getInstance().onServo1To50();
                invalidate();
                break;
            case BUTTON_SERVO_1_TO_100:
                setOutput("Sending: servo 1 to 100%");
                MainWindowManager.getInstance().onServo1To100();
                invalidate();
                break;
            case BUTTON_SERVO_2_TO_0:
                setOutput("Sending: servo 2 to 0%");
                MainWindowManager.getInstance().onServo2To0();
                invalidate();
                break;
            case BUTTON_SERVO_2_TO_50:
                setOutput("Sending: servo 2 to 50%");
                MainWindowManager.getInstance().onServo2To50();
                invalidate();
                break;
            case BUTTON_SERVO_2_TO_100:
                setOutput("Sending: servo 2 to 100%");
                MainWindowManager.getInstance().onServo2To100();
                invalidate();
                break;
        }
    }

    /* package */ void setOutput(@NonNull final String output) {
        mJLabel.setText("   " + output);
    }

    private void addJButtonToPanel(final JPanel panel, final String jButtonText) {
        final JButton jButton = new JButton(jButtonText);
        jButton.addActionListener(this);
        jButton.setBackground(new Color(44, 44, 44));
        jButton.setForeground(Color.WHITE);
        jButton.setFocusPainted(false);
        jButton.setFont(new Font("Tahoma", Font.BOLD, 12));
        panel.add(jButton);
    }
}
