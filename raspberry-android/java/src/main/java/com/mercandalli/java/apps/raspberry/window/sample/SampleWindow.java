package com.mercandalli.java.apps.raspberry.window.sample;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.Line2D;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

/* package */ class SampleWindow extends JFrame implements ActionListener {

    private static final String BUTTON_1 = "BUTT-1";
    private static final String BUTTON_2 = "BUTT-2";

    public SampleWindow() {
        final JPanel panel = new JPanel();
        getContentPane().add(panel);
        setSize(450, 450);

        final JButton button1 = new JButton(BUTTON_1);
        button1.addActionListener(this);
        panel.add(button1);

        final JButton button2 = new JButton(BUTTON_2);
        button2.addActionListener(this);
        panel.add(button2);
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);
        Graphics2D g2 = (Graphics2D) g;
        Line2D lin = new Line2D.Float(100, 100, 250, 260);
        g2.draw(lin);
    }

    @Override
    public void actionPerformed(final ActionEvent e) {
        switch (e.getActionCommand()) {
            case BUTTON_1:

                break;
            case BUTTON_2:

                break;
        }
    }
}
