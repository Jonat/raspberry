package com.mercandalli.java.apps.raspberry.api;

import android.support.annotation.FloatRange;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;
import com.mercandalli.java.apps.raspberry.log.Log;
import com.mercandalli.java.apps.raspberry.network.NetworkManager;
import com.mercandalli.java.shared.api.NucCommandApi;
import com.mercandalli.java.shared.model.Hardware;
import com.mercandalli.java.shared.model.PhpCommand;
import com.mercandalli.java.shared.model.PhpResponse;
import com.mercandalli.java.shared.utils.CreateCommandUtils;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

public class PhpApiManager implements NucCommandApi, NetworkManager.OnPostListener {

    private static final String TAG = "PhpApiManager";

    private static final String URL_ROOT =
            "http://mercandalli.com/raspberry/raspberry-android/api/robotics";

    private static final String EMPTY_COMMAND =
            "{\"raspberry-content\":{\"content\":{\"hardware\":[]},\"succeed\":true}}";

    private static final int REQUEST_ID_OFFSET = 100;

    @NonNull
    private static final Type TYPE_RESPONSE = new TypeToken<PhpResponse>() {
    }.getType();

    @Nullable
    private static PhpApiManager sInstance;

    @NonNull
    public static PhpApiManager getInstance() {
        if (sInstance == null) {
            sInstance = new PhpApiManager();
        }
        return sInstance;
    }

    @NonNull
    private final CryptManager mCryptManager = CryptManager.getInstance();

    @NonNull
    private final Gson mGson = new Gson();

    @NonNull
    private final List<OnWriteLedListener> mOnWriteLedListeners = new ArrayList<>();

    @NonNull
    private final List<OnWriteServoListener> mOnWriteServoListeners = new ArrayList<>();

    private PhpApiManager() {
        NetworkManager.getInstance().addOnPostListener(this);
    }

    @Override
    public void sendCommand(@NonNull final PhpCommand command) {
        sendCommand(-1, command);
    }

    @Override
    public void sendCommand(@Hardware.HardwareLedId final int id, @NonNull final PhpCommand command) {
        Log.d(TAG, "sendCommand() method with this command: " + command);
        post(id, URL_ROOT, mGson.toJson(command));
    }

    @Override
    public void writeLed(@Hardware.HardwareLedId final int id, final boolean on) {
        sendCommand(REQUEST_ID_OFFSET + id, CreateCommandUtils.createWriteLedCommand(id, on));
    }

    @Override
    public void writeServo(
            @Hardware.HardwareServoId final int id,
            @FloatRange(from = 0.0, to = 1.0) final float value) {
        sendCommand(REQUEST_ID_OFFSET + id, CreateCommandUtils.createWriteServoCommand(id, value));
    }

    @Override
    public boolean onPostSucceeded(final int requestId, @NonNull final String response) {
        PhpResponse phpResponse = null;
        try {
            phpResponse = mGson.fromJson(response, TYPE_RESPONSE);
        } catch (JsonSyntaxException ignored) {
        }
        switch (requestId) {
            case REQUEST_ID_OFFSET + Hardware.ID_LED_1:
                if (phpResponse != null && phpResponse.isSucceeded()) {
                    notifyOnWriteLedSucceeded();
                } else if (phpResponse == null) {
                    notifyOnWriteLedFailed("Cannot parse the json response. Here the response: " +
                            response);
                } else {
                    notifyOnWriteLedFailed("The raspberry is " +
                            (phpResponse.isRaspberryConnected() ? "" : "not ") + "connected.");
                }
            case REQUEST_ID_OFFSET + Hardware.ID_SERVO_1_DIRECTION:
            case REQUEST_ID_OFFSET + Hardware.ID_SERVO_2_ENGINE:
                if (phpResponse != null && phpResponse.isSucceeded()) {
                    notifyOnWriteServoSucceeded();
                } else if (phpResponse == null) {
                    notifyOnWriteServoFailed("Cannot parse the json response. Here the response: " +
                            response);
                } else {
                    notifyOnWriteServoFailed("The raspberry is " +
                            (phpResponse.isRaspberryConnected() ? "" : "not ") + "connected.");
                }
                break;
        }
        return false;
    }

    @Override
    public boolean onPostFailed(final int requestId) {
        switch (requestId) {
            case REQUEST_ID_OFFSET + Hardware.ID_LED_1:
            case REQUEST_ID_OFFSET + Hardware.ID_SERVO_1_DIRECTION:
                notifyOnWriteLedFailed("Unknown");
                break;
        }
        return false;
    }

    public boolean addOnWriteLedListener(@Nullable final OnWriteLedListener onPostListener) {
        synchronized (mOnWriteLedListeners) {
            //noinspection SimplifiableIfStatement
            if (onPostListener == null ||
                    mOnWriteLedListeners.contains(onPostListener)) {
                // We don't allow to register null listener
                // And a listener can only be added once.
                return false;
            }
            return mOnWriteLedListeners.add(onPostListener);
        }
    }

    public boolean removeOnWriteLedListener(final OnWriteLedListener onPostListener) {
        synchronized (mOnWriteLedListeners) {
            return mOnWriteLedListeners.remove(onPostListener);
        }
    }

    private void notifyOnWriteLedSucceeded() {
        final ListIterator<OnWriteLedListener> onPostListenerListIterator = mOnWriteLedListeners.listIterator();
        while (onPostListenerListIterator.hasNext()) {
            final OnWriteLedListener next = onPostListenerListIterator.next();
            if (next.onWriteLedSucceeded()) {
                onPostListenerListIterator.remove();
            }
        }
    }

    private void notifyOnWriteLedFailed(@NonNull final String errorMessage) {
        Log.d(TAG, "Post failed. " + errorMessage);
        final ListIterator<OnWriteLedListener> onPostListenerListIterator = mOnWriteLedListeners.listIterator();
        while (onPostListenerListIterator.hasNext()) {
            final OnWriteLedListener next = onPostListenerListIterator.next();
            if (next.onWriteLedFailed(errorMessage)) {
                onPostListenerListIterator.remove();
            }
        }
    }

    public boolean addOnWriteServoListener(@Nullable final OnWriteServoListener onWriteServoListener) {
        synchronized (mOnWriteServoListeners) {
            //noinspection SimplifiableIfStatement
            if (onWriteServoListener == null ||
                    mOnWriteServoListeners.contains(onWriteServoListener)) {
                // We don't allow to register null listener
                // And a listener can only be added once.
                return false;
            }
            return mOnWriteServoListeners.add(onWriteServoListener);
        }
    }

    public boolean removeOnWriteServoListener(final OnWriteServoListener onWriteServoListener) {
        synchronized (mOnWriteServoListeners) {
            return mOnWriteServoListeners.remove(onWriteServoListener);
        }
    }

    private void notifyOnWriteServoSucceeded() {
        final ListIterator<OnWriteServoListener> onPostListenerListIterator = mOnWriteServoListeners.listIterator();
        while (onPostListenerListIterator.hasNext()) {
            final OnWriteServoListener next = onPostListenerListIterator.next();
            if (next.onWriteServoSucceeded()) {
                onPostListenerListIterator.remove();
            }
        }
    }

    private void notifyOnWriteServoFailed(@NonNull final String errorMessage) {
        Log.d(TAG, "Post failed. " + errorMessage);
        final ListIterator<OnWriteServoListener> onPostListenerListIterator = mOnWriteServoListeners.listIterator();
        while (onPostListenerListIterator.hasNext()) {
            final OnWriteServoListener next = onPostListenerListIterator.next();
            if (next.onWriteServoFailed(errorMessage)) {
                onPostListenerListIterator.remove();
            }
        }
    }

    private void post(final int requestId, @NonNull final String httpsURL, @Nullable final String queryJson) {
        NetworkManager.getInstance().httpPost(requestId, httpsURL, mCryptManager.encrypt(queryJson));
    }

    public interface OnWriteLedListener {

        boolean onWriteLedSucceeded();

        boolean onWriteLedFailed(@NonNull final String errorMessage);
    }

    public interface OnWriteServoListener {

        boolean onWriteServoSucceeded();

        boolean onWriteServoFailed(@NonNull final String errorMessage);
    }
}
